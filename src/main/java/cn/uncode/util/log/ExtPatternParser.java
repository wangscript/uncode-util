package cn.uncode.util.log;

// 参考 http://blog.csdn.net/u010162887/article/details/51736637
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.helpers.FormattingInfo;
import org.apache.log4j.helpers.PatternConverter;
import org.apache.log4j.helpers.PatternParser;
import org.apache.log4j.spi.LoggingEvent;

public class ExtPatternParser extends PatternParser {
	
	public static String MARK_SID="sid";// 服务标识。一个请求，一个新的服务标识
	public static String MARK_IP="ip";// 服务标识。一个请求，一个新的服务标识

	public static final ThreadLocal<Map<String, Object>> TH_LOCAL = new ThreadLocal<Map<String, Object>>(){
		@Override
        protected Map<String, Object> initialValue() {
            return new ConcurrentHashMap<String, Object>();
        } 
	};

	public static void setCurrentValue(String key, Object value) {
		Map<String, Object> map = TH_LOCAL.get();
		map.put(key, value);
		
	}

	public ExtPatternParser(String pattern) {
		super(pattern);
	}

	public void finalizeConverter(char c) {
		if (c == '#') {
			String exs = super.extractOption();
			addConverter(new ExrPatternConverter(formattingInfo, exs));
			currentLiteral.setLength(0);
		} else {
			super.finalizeConverter(c);
		}
	}

	private class ExrPatternConverter extends PatternConverter {

		private String cfg;

		ExrPatternConverter(FormattingInfo formattingInfo, String cfg) {
			super(formattingInfo);
			this.cfg = cfg;
		}

		public String convert(LoggingEvent event) {
			Map<String, Object> valueMap = TH_LOCAL.get();
			if (valueMap != null) {
				Object value = valueMap.get(cfg);
				if (value != null) {
					return String.valueOf(value);
				}
			}
			return "";
		}
	}
}
