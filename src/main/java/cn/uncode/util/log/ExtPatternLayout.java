package cn.uncode.util.log;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.helpers.PatternParser;  
  
public class ExtPatternLayout extends PatternLayout {  
    public ExtPatternLayout() {  
        this(DEFAULT_CONVERSION_PATTERN);  
    }  
  
    public ExtPatternLayout(String pattern) {  
        super(pattern);  
    }  
  
    @Override  
    public PatternParser createPatternParser(String pattern) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("[host: %#{xp}, requestId: %#{sid}]");
    	sb.append(pattern == null ? DEFAULT_CONVERSION_PATTERN : pattern);
        return new ExtPatternParser(sb.toString());
    }  
}  
